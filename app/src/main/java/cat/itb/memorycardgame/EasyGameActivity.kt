package cat.itb.memorycardgame

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.easy_card_manager.*
import kotlinx.android.synthetic.main.win_layout.*
import kotlinx.android.synthetic.main.lose_layout.*
import cat.itb.memorycardgame.R.mipmap.*
import java.text.FieldPosition

private const val TAG = "EasyGameActivity"

class EasyGameActivity : AppCompatActivity() {


    private lateinit var buttonCartes: List<ImageButton>
    private lateinit var cartes: List<Carta>

    private var parellesTrobades = 0
    private var intentsRestants = 3

    private var resultIntents = 0
    private var resultParelles = 0
    private var totalResult = 0


    private var indexOfSingleSelectedCard: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.easy_card_manager)

        val images = mutableListOf(yitan_foreground, garnet_foreground, vivi_foreground)
        //afageix els parells d'imatges amb el metode parells
        images.addAll(images)
        //randomitza les imatges
        images.shuffle()

        buttonCartes = listOf(
            imageButton,
            imageButton2,
            imageButton3,
            imageButton4,
            imageButton5,
            imageButton6
        )
        cartes = buttonCartes.indices.map { index ->
            Carta(images[index])
        }

        buttonCartes.forEachIndexed { index, bCarta ->
            bCarta.setOnClickListener {
                Log.i(TAG, "Button clicked.")
                //actualitzar models
                updateModels(index)
                //actualitzar la UI del joc
                updateViews()

            }
        }


    }

    private fun updateViews() {
        cartes.forEachIndexed { index, carta ->
            val bCarta = buttonCartes[index]
            if (carta.isMatched) {
                bCarta.alpha = 0.1f
            }
            bCarta.setImageResource(if (carta.girada) carta.id else back_foreground)
        }

    }

    fun restaurarCartes() {
        for (carta in cartes) {
            if (!carta.isMatched) {
                carta.girada = false
            }
        }
    }

    private fun updateModels(position: Int) {
        val carta = cartes[position]

        // error comprovar:
        if (carta.girada) {
            Toast.makeText(this, "moviment invalid!", Toast.LENGTH_SHORT).show()
            return
        }
        //0 cartes girades => restaura la carta + gira la carta seleccionada
        //1 carta girada => gira la carta seleccionada + comproba si les cartes seleccionades son iguals
        //2 cartes girades => restaura les cartes + gira la carta seleccionada
        if (indexOfSingleSelectedCard == null) {
            //0 o 2 cartes seleccionades previament
            restaurarCartes()
            indexOfSingleSelectedCard = position


        } else {
            //si la carta  ha sigut seleccionada previament
            confirmarParella(indexOfSingleSelectedCard!!, position)
            indexOfSingleSelectedCard = null
        }
        carta.girada = !carta.girada

    }


    private fun confirmarParella(position1: Int, position2: Int) {

        if (cartes[position1].id == cartes[position2].id) {
            Toast.makeText(this, "parella trobada", Toast.LENGTH_SHORT).show()
            cartes[position1].isMatched = true
            cartes[position2].isMatched = true

            parellesTrobades++
            pair_text.text = "Pairs: " + parellesTrobades + "/3"
            if (parellesTrobades == 3) {
                Toast.makeText(this, "You Win!", Toast.LENGTH_SHORT).show()
                setContentView(R.layout.win_layout)

                resultParelles = parellesTrobades * 5
                resultIntents = intentsRestants * 7
                totalResult = resultParelles + resultIntents
                pairsTotal.text = "Pairs: "+ resultParelles
                movementsTotal.text = "Movements: " + resultIntents
                totalText.text = "TOTAL: " + totalResult
                buttonMenu.setOnClickListener(){
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)

                }
                retryGame.setOnClickListener(){
                    val intent2 = Intent(this,EasyGameActivity::class.java)
                   // finish()
                    startActivity(intent2)

                }
            }
        } else {
            intentsRestants--
            moves_text.text = "Moves: " + intentsRestants
            if (intentsRestants == 0) {
                Toast.makeText(this, "Game Over", Toast.LENGTH_SHORT).show()
                setContentView(R.layout.lose_layout)
                resultParelles = parellesTrobades * 5
                totalResult = resultParelles
                pairsTotal2.text = "Pairs: "+ resultParelles
                totalText2.text = "TOTAL: " + totalResult
                buttonMenu2.setOnClickListener(){
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)

                }
                retryGame2.setOnClickListener(){
                    val intent2 = Intent(this,EasyGameActivity::class.java)
                   // finish()
                    startActivity(intent2)

                }
            }
        }
    }


}