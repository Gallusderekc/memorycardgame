package cat.itb.memorycardgame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.util.Log
import android.widget.Button
import com.google.android.material.dialog.MaterialAlertDialogBuilder



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.splashScreenTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_menu)
        Log.d("DEBUG: ", "onCreate()")

        val difficults = resources.getStringArray(R.array.select_difficult)

        val arrayAdapter = ArrayAdapter(this, R.layout.dropdown_item, difficults)

        val autocompleteTV = findViewById<AutoCompleteTextView>(R.id.autoCompleteTextView)

       val  helpButton = findViewById<Button>(R.id.help_button)

        val playButton = findViewById<Button>(R.id.play_button)


        autocompleteTV.setAdapter(arrayAdapter)


        playButton.setOnClickListener(){
            if (autocompleteTV.text.toString()=="Easy"){
                val intent = Intent(this, EasyGameActivity::class.java)
                startActivity(intent)
            }else if (autocompleteTV.text.toString()=="Normal"){
                val intent = Intent(this, NormalGameActivity::class.java)
                startActivity(intent)
            }


        }









        helpButton.setOnClickListener{
            MaterialAlertDialogBuilder(this)
                .setTitle("Final Fantasy IX Tetra Memory")
                .setMessage("You have to tap the pair of cards of the same type to win the game in the less time as possible")
                .setPositiveButton("Ok", null)
                .show()
        }



/*
https://www.youtube.com/watch?v=741l_fPKL3Y
https://www.geeksforgeeks.org/exposed-drop-down-menu-in-android/
https://material.io/components/dialogs/android#simple-dialog
https://www.youtube.com/watch?v=DsN0FBrHRc8
 */
    }

    override fun onStart() {
        super.onStart()
        Log.d("DEBUG: ", "onStart()")
    }

    override fun onResume() {
        super.onResume()
        Log.d("DEBUG: ", "onResume()")
    }
    override fun onPause() {
        super.onPause()
        Log.d("DEBUG: ", "onPause()")
    }

    override fun onStop() {
        super.onStop()
        Log.d("DEBUG: ", "onStop()")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("DEBUG: ", "onDestroy()")
    }



}