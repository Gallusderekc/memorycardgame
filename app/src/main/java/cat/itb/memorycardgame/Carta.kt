package cat.itb.memorycardgame

data class Carta(
    val id: Int,
    var girada: Boolean = false,
    var isMatched: Boolean = false
)
